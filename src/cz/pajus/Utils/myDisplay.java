package cz.pajus.Utils;

import org.apache.http.util.LangUtils;

import android.app.Activity;
import android.view.Display;
import android.view.Surface;

public class myDisplay {

	public static final int LANDSCAPE = 0;
	public static final int PORTRAIT = 1;
	
	private static Display mDisplay = null;
	
	public static void initDisplay(Activity ac){
		mDisplay = ac.getWindowManager().getDefaultDisplay();
	}
	
	
	
	public static int getWidth(){
		if(mDisplay != null){
			return mDisplay.getWidth();
		}
		return -1;
	}
	
	public static int getHeight(){
		if(mDisplay != null){
			return mDisplay.getHeight();
		}
		return -1;
	}
	
	/*
	
	*/
	
	public static int getRotation(){
		if(mDisplay != null){
			int rotation = mDisplay.getRotation();
			if(rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180){
				return LANDSCAPE;
			}
			else if(rotation == Surface.ROTATION_270 || rotation == Surface.ROTATION_90){
				return PORTRAIT;
			}
		}
		return -1;
	}
}
