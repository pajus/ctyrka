package cz.pajus.Utils;

import android.util.Log;

/**
 * MD - myDebug class
 * 
 * 
 * @author pajus
 *
 */
public class MD {
	public static boolean LOG_ENABLED = true;
	public static final String APPTAG = "CTYRKA";
	public static final String SHORT_APPTAG = "CTY";
	
	public static void d(String msg){
		if(LOG_ENABLED){
			Log.d(APPTAG,msg);
		}
	}
	
	public static void d(String tag, String msg){
		if(LOG_ENABLED){
			Log.d(SHORT_APPTAG + "_" + tag, msg);
		}
	}
	
	public static void e(String msg){
		if(LOG_ENABLED){
			Log.e(APPTAG,msg);
		}
	}
	
	public static void e(String tag, String msg){
		if(LOG_ENABLED){
			Log.e(SHORT_APPTAG + "_" + tag, msg);
		}
	}


	public static void disableLog(){
		LOG_ENABLED = false;
	}
}
