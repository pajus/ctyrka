package cz.pajus.Utils;

import android.content.Context;
import android.widget.Toast;

public class myToast {

	private static Context mContext;
	
	public static void setContext(Context c){
		mContext = c;
	}
	
	public static void show(String msg){
		int duration = Toast.LENGTH_SHORT;
		if(msg.length() > 100){
			duration = Toast.LENGTH_LONG;
		}
		Toast.makeText(mContext, msg, duration).show();
	}
	
	public static void show(Context mC, String msg){
		int duration = Toast.LENGTH_SHORT;
		if(msg.length() > 100){
			duration = Toast.LENGTH_LONG;
		}
		Toast.makeText(mC, msg, duration).show();
	}
}
