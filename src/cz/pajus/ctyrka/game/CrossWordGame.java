package cz.pajus.ctyrka.game;

import java.util.ArrayList;
import java.util.Random;

import cz.pajus.Utils.MD;
import cz.pajus.ctyrka.Widget.Grid;

import android.provider.UserDictionary.Words;
import android.util.Log;

public class CrossWordGame {
	
	Character GameGrid[] = null;

	private static int WIDTH;
	private static int HEIGHT;
	public static int ELEMENTS;

	private static int WORDLEN = 4;

	Random rand = new Random(13597);

	private ArrayList<Integer> SelectedLetters = new ArrayList<Integer>();
	private ArrayList<String> Words = new ArrayList<String>();

	public void initGame(int width, int height) {
		setWidth(width);
		setHeight(height);
		ELEMENTS = width * height;

		GameGrid = new Character[ELEMENTS];
		fillGrid();
	}

	public void setWidth(int width){
		WIDTH = width;
	}
	public void setHeight(int height){
		HEIGHT = height;
	}
	public int setWordLen(int wordLen) {
		WORDLEN = wordLen;
		return WORDLEN;
	}
	public int getWordLen(){
		return WORDLEN;
	}
	public int getWidth(){
		return WIDTH;
	}
	public int getHeight(){
		return HEIGHT;
	}
	
	public Character getLetter(int index){
		return GameGrid[index];
	}

	private void fillGrid() {
		// Random fill grid
		if (GameGrid != null) {
			for (int i = 0; i < ELEMENTS; ++i) {
				GameGrid[i] = new Character((char)(rand.nextInt((90 - 65) + 1) + 65));
				// 65 = A
				// 90 = Z
			}
		}
	}

	public Character[] getGrid() {
		return GameGrid;
	}
	
	/*
	 * @return if button is pressed
	 */
	
	public boolean pressLetter(int index){
		
		if(is8Neigh(index)){
			SelectedLetters.add(index);
			return true;
		}
		
		return false;
	}
	
	public boolean isWordAndClean(){
		if(isWordComplete()){
			addWord();
			cleanLetters();
			MD.d(Words.toString());
			return true;
		}
		return false;
	}
	
	public boolean isWordComplete(){
		if(SelectedLetters.size() >= WORDLEN){
			return true;
		}
		return false;
	}
	
	public String addWord(){
		String word = "";
		for( int i : SelectedLetters){
			word += GameGrid[i];
		}
		Words.add(word);
		return word;
	}
	
	public void cleanLetters(){
		SelectedLetters.clear();
	}
	
	public String getLastWord(){
		return Words.get(Words.size()-1);
	}
	
	public boolean isButtonClicked(int index){
		return false;
	}

	public void printGrid() {
		for (int i = 0; i < HEIGHT; i++) {
			String row = "";
			for (int j = 0; j < WIDTH; j++) {
				if (j > 0)
					row += ", ";
				row += Character.toString(GameGrid[i*WIDTH + j]);
			}
			Log.d("Grid " +i+":", row);
		}
		
		String l = "";
		for(int i = 0; i < ELEMENTS; ++i){
			if (i > 0)
				l += ", ";
			l += GameGrid[i].toString();
		}
		MD.d(l);
	}
	
	private boolean is8Neigh(int index) {
		int id = index;
		
		// If we have the first button
		if (SelectedLetters.size() == 0)
			return true;

		int lastLabel = SelectedLetters.get(SelectedLetters.size() - 1);

		int x = (int)Math.floor((double)id / WIDTH);
		int y = id % WIDTH;

		int _x = (int)Math.floor((double)lastLabel / WIDTH);
		int _y = lastLabel % WIDTH;
		

		if (x - 1 == _x && y == _y)
			return true;
		if (x + 1 == _x && y == _y)
			return true;
		if (x == _x && y - 1 == _y)
			return true;
		if (x == _x && y + 1 == _y)
			return true;

		if (x - 1 == _x && y - 1 == _y)
			return true;
		if (x + 1 == _x && y + 1 == _y)
			return true;
		if (x - 1 == _x && y + 1 == _y)
			return true;
		if (x + 1 == _x && y - 1 == _y)
			return true;

		return false;
	}

	private boolean is4Neigh(int index) {
		int id = index;
		
		// If we have the first button
		if (SelectedLetters.size() == 0)
			return true;

		int lastLabel = SelectedLetters.get(SelectedLetters.size() - 1);

		int x = (int)Math.floor((double)id / WIDTH);
		int y = id % WIDTH;

		int _x = (int)Math.floor((double)lastLabel / WIDTH);
		int _y = lastLabel % WIDTH;

		if (x - 1 == _x && y == _y)
			return true;
		if (x + 1 == _x && y == _y)
			return true;
		if (x == _x && y - 1 == _y)
			return true;
		if (x == _x && y + 1 == _y)
			return true;

		return false;
	}
}
