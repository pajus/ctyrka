package cz.pajus.ctyrka.Activity;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cz.pajus.Utils.myDisplay;
import cz.pajus.ctyrka.R;
import cz.pajus.ctyrka.Widget.Grid;

public class Menu extends Activity {

	Grid mGrid = null;
	
	long startTime = 0;
	long stopTime = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		setFonts();
		setButtons();
		
		
	}

	int Buttons[] = {R.id.menu_NewGame, R.id.menu_27challenge, R.id.menu_credits,
			R.id.menu_info, R.id.menu_multiplayer, R.id.menu_rate, R.id.menu_settings};
	private void setButtons(){
		
	}
	private void setFonts(){
		
		Typeface tf = Typeface.createFromAsset(getAssets(), "goodtime.ttf");
		for( int id : Buttons){
			Button b = (Button) findViewById(id);
			b.setTypeface(tf);
		}
	}
	
	public void click_27(View v){
		 Intent intent = new Intent(this, cz.pajus.ctyrka.Activity.Game.class);
		 startActivity(intent);
	}
	
	public void click_new_game(View v){
		click_27(v);
	}
}
