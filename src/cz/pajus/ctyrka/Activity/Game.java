package cz.pajus.ctyrka.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import cz.pajus.Utils.MD;
import cz.pajus.Utils.myDisplay;
import cz.pajus.ctyrka.R;
import cz.pajus.ctyrka.Widget.Grid;
import cz.pajus.ctyrka.Widget.WordList;
import cz.pajus.ctyrka.game.CrossWordGame;

public class Game extends Activity {

	Grid mGrid = null;

	CrossWordGame mGame = new CrossWordGame();
	WordList mWordList;
	
	long startTime = 0;
	long stopTime = 0;

	int WORDLEN = 4;
	int X = 5;
	int Y = 5;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_layout);
		
		myDisplay.initDisplay(this);
		
		mGame.initGame(X,Y);
		mGame.setWordLen(WORDLEN);
		mGame.printGrid();
		Grid mGrid = (Grid) findViewById(R.id.game_grid);
		mGrid.initGrid(mGame);
		
		mWordList = (WordList) findViewById(R.id.game_wordlist);
		mWordList.initWordList(mGame.getWordLen());
	}
	
	public void startGame(View view) {
		// TIMER
		startTime = System.currentTimeMillis();
	}


	public void stopGame(View view) {
		if (mGame != null) {

			stopTime = System.currentTimeMillis();

			long TimeInMilis = stopTime - startTime;

			long second = (TimeInMilis / 1000) % 60;
			long minute = (TimeInMilis / (1000 * 60)) % 60;
			long hour = (TimeInMilis / (1000 * 60 * 60)) % 24;

			String time = String.format("%02d:%02d:%02d", hour, minute, second);

			MD.d("Game takes: " + time);
			
			mWordList.cleanList();
			//@ TODO evaluate game!


		}
	}


}
