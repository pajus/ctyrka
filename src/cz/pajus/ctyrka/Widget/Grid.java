package cz.pajus.ctyrka.Widget;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.GridLayout;
import android.util.AttributeSet;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import cz.pajus.Utils.MD;
import cz.pajus.Utils.myDisplay;
import cz.pajus.ctyrka.game.CrossWordGame;

public class Grid extends GridLayout {

	public static final int GRID_ID = 146310; // @ TODO set unique from
												// resources ie

	private int BUTTON_SIZE = 128;
	private static final int BTN_MARGIN = 5;

	CrossWordGame mGame = null;
	WordList mWordList = null;

	Context mContext = null;

	ArrayList<myButton> mButtons = new ArrayList<myButton>();

	public Grid(Context c, AttributeSet attrs) {
		super(c, attrs);
		mContext = c;
	}

	public Grid(Context c, CrossWordGame game) {
		super(c);

		mContext = c;
		mGame = game;
		super.setId(GRID_ID);

		super.setColumnCount(mGame.getWidth());
		super.setRowCount(mGame.getHeight());

		setVisual(c);
	}

	public void initGrid(CrossWordGame game) {
		mGame = game;

		super.setColumnCount(mGame.getWidth());
		super.setRowCount(mGame.getHeight());

		setVisual(mContext);
	}

	private int calcButtonSize() {
		int btnSize = 0;

		if (myDisplay.getRotation() == myDisplay.LANDSCAPE) {
			btnSize = myDisplay.getHeight() / (mGame.getHeight());
			btnSize = btnSize - (mGame.getHeight() * BTN_MARGIN);
		} else {
			btnSize = myDisplay.getWidth() / (mGame.getWidth());
			btnSize = btnSize - (mGame.getWidth() * BTN_MARGIN);
		}
		MD.d("BTNSIZE: " + btnSize);

		return btnSize;
	}

	private void setVisual(Context c) {
		super.setPadding(20, 10, 20, 10);
		int elements = mGame.ELEMENTS;

		int btnSize = calcButtonSize();

		for (int i = 0; i < elements; ++i) {
			myButton b = new myButton(c, i);
			b.setText(mGame.getLetter(i));
			// Set Layout
			GridLayout.LayoutParams params = new GridLayout.LayoutParams();
			params.setMargins(BTN_MARGIN, BTN_MARGIN, BTN_MARGIN, BTN_MARGIN);
			params.height = btnSize;
			params.width = btnSize;
			b.setLayoutParams(params);
			// set on touch listener
			b.setOnTouchListener(onTouch);
			b.setOnClickListener(onClick);
			mButtons.add(b);
			super.addView(b);
		}
	}

	public void setWordList(WordList wordList) {
		this.mWordList = wordList;
	}

	private OnClickListener onClick = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			MD.d("onClick");
		}
	};

	private OnTouchListener onTouch = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// Filter unnecessary actions :)
			if (event.getAction() == MotionEvent.ACTION_DOWN)
				return true;
			if (event.getAction() != MotionEvent.ACTION_UP)
				return false;

			myButton b = (myButton) v;
			if (mGame.pressLetter(b.getIndex())) {
				b.setPressed(true);
				// Just cosmetics
			}

			if (mGame.isWordAndClean()) {
				// Unset BUttons
				for (myButton mb : mButtons) {
					mb.setPressed(false);
				}
				// Trigger interface to WordComplete - > WordList
				WordComplete.getInstance().completeWord(mGame.getLastWord());
			}
			return true;
		}

	};

	public Grid(Context context, int width, int height,
			LayoutInflater inflater, final Activity ac) {
		super(context);

		// Set rows and cols
		super.setRowCount(height);
		super.setColumnCount(width);
		super.setPadding(20, 10, 80, 10);
		//
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		int size = display.getWidth() / 3;
		this.BUTTON_SIZE = size;
	}

}