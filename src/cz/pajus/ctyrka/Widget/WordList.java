package cz.pajus.ctyrka.Widget;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import cz.pajus.Utils.MD;
import cz.pajus.Utils.myDisplay;
import cz.pajus.ctyrka.R;
import cz.pajus.ctyrka.Widget.WordComplete.OnWordCompleteListener;

public class WordList extends ScrollView implements OnWordCompleteListener {

	public interface onWordCompleteListener {
		void onWordComplete(String word);
	}

	static final ArrayList<String> mWords = new ArrayList<String>();
	LinearLayout container;
	Context mContext;
	private int mWordLen;

	public WordList(Context c, AttributeSet attrs) {
		super(c, attrs);

		WordComplete.getInstance().setListener(this);
		mContext = c;
		super.setBackgroundResource(R.drawable.word_list);

	}

	//@ TODO - on rotate!
	
	public void initWordList(int wordLen) {

		mWordLen = wordLen;

		LinearLayout ll = new LinearLayout(mContext);
		ll.setOrientation(LinearLayout.VERTICAL);
		LayoutParams linLayoutParam = new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		linLayoutParam.gravity = Gravity.CENTER_HORIZONTAL;
		ll.setLayoutParams(linLayoutParam);

		if (ll.getLayoutParams() == null) {
			MD.e("WTF");
		}
		container = ll;

		Point listSize = getListSize();
		ViewGroup.LayoutParams params = super.getLayoutParams();
		params.width = listSize.x;
		params.height = listSize.y;
		super.setLayoutParams(params);

		if(mWords.size() > 0){
			for(String w : mWords){
				addWord(mContext, w);
			}
		}
		super.addView(ll);
	}
		
	public void cleanList(){
		mWords.clear();
		if(container.getChildCount() > 0){
			container.removeAllViews();
		}
	}

	private Point getListSize() {
		Point size = new Point(0, 0);

		if (myDisplay.getRotation() == myDisplay.LANDSCAPE) {
			size.x = 60 * mWordLen;
			size.y = myDisplay.getHeight() / 2;
		} else {
			size.x = 60 * mWordLen;
			size.y = myDisplay.getHeight() / 4;
		}

		MD.d("point size: " + size.toString() + "  >" + mWordLen);
		return size;
	}

	@Override
	public void wordComplete(String word) {
		addWord(mContext, word);
		mWords.add(word);
	}

	private void addWord(Context c, String word) {
		TextView tv = new TextView(c);
		tv.setText(word);
		tv.setTextColor(Color.WHITE);
		// tv.setBackgroundColor(getResources().getColor(R.color.bacground));
		tv.setTypeface(Typeface.MONOSPACE);
		tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
		// tv.setPadding(20, 0, 20, 0);

		container.addView(tv);
	}

}
