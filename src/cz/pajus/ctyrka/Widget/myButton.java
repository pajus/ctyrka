package cz.pajus.ctyrka.Widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import cz.pajus.Utils.MD;
import cz.pajus.ctyrka.R;

public class myButton extends Button {
	public int index;

	public myButton(Context c, int index) {
		super(c);
		this.index = index;
		this.setButtonStyle(c);
		super.setId(index);
		super.setBackgroundResource(R.drawable.square_button);
	}

	public void setText(Character c) {
		super.setText(c.toString());
	}

	private void setButtonStyle(Context c) {
		super.setTextColor(Color.WHITE);
		super.setTypeface(Typeface.MONOSPACE);
		super.setTextSize(TypedValue.COMPLEX_UNIT_SP, 50);
		super.setPadding(40, 20, 40, 20);

		super.setGravity(Gravity.CENTER);
	}
	
	public void setButtonPressed(int index, boolean pressed){
		
	}
	public int getIndex(){
		return this.index;
	}
	
}
