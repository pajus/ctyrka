package cz.pajus.ctyrka.Widget;

public class WordComplete {
	
	public interface OnWordCompleteListener{
		void wordComplete(String word);
	}
	
	private static WordComplete mInstance;
	private OnWordCompleteListener mListener;
	
	private WordComplete() {}
	
	public static WordComplete getInstance() {
		if(mInstance == null){
			mInstance = new WordComplete();
		}
		return mInstance;
	}
	
	public void setListener(OnWordCompleteListener listener){
		mListener = listener;
	}

	public void completeWord(String word){
		if(mListener != null){
			mListener.wordComplete(word);
		}
	}
}
